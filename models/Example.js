module.exports = (sequelize, DataTypes) => {
  const Example = sequelize.define('Example', {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: DataTypes.STRING,
      date: DataTypes.DATETIME
    },
    {
      freezeTableName: true,
      tableName: 'example_table_name'
    }
  )

  return Example
}
